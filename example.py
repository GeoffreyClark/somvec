import numpy as np
import pandas as pd

from somvec import SOM
#dataset
from sklearn.datasets import load_iris

if __name__ == '__main__':
    #load dataset and create dataframe
    iris = load_iris()
    target = iris['target']
    cols = ['sepalLength','sepalWidth','petalLength','petalWidth']
    df = pd.DataFrame(iris['data'], columns=cols)
    print('Shape of dataset: ', df.shape)

    #scenarios to test:
    show_plot = True
    ScenariosToRun = {
        'Base': True
        ,'Quantisation_Error': False
        ,'Hypyercube': False
        ,'Non_PBC': False
        ,'Other_Params': False
        ,'Semi_Supervised': False
        ,'Fully_Supervised': False
        ,'OtherFunctionality': False
    }

    if ScenariosToRun['Base']:
        #Base scenario
        scenarioName = 'Base'
        #create class with some of the default parameters to demonstrate parameterisability
        som = SOM(df, scenarioName=scenarioName, save=True, savePath='SavedModels/', load=False
            , loadPath='SavedModels/', saveImages=True, imagePath='Images/', saveClusters=True, clusterPath='Clusters/')
        som.fit(show_plot=show_plot)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['Quantisation_Error']:
        #Quantization Error scenario
        scenarioName = 'Quantisation_Error'
        som = SOM(df, scenarioName=scenarioName, errType='Quantisation')
        som.fit(show_plot=show_plot)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['Hypyercube']:
        #Hypercube Intialisation scenario
        scenarioName = 'Hypyercube'
        som = SOM(df, scenarioName=scenarioName, weightInitialisation='hypercube')
        som.fit(show_plot=show_plot)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['Non_PBC']:
        #Non_PBC scenario (without periodic boundary conditions so that the cube does not wrap around)
        scenarioName = 'Non_PBC'
        som = SOM(df, scenarioName=scenarioName, PBC=False)
        som.fit(show_plot=show_plot)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['Other_Params']:
        #Other_params scenario: change some of the training params to suit the specific problem
        scenarioName = 'Other_Params'
        som = SOM(df, scenarioName=scenarioName, epochs=200, initialLearnRate=0.2
            , initialKernelWidth=10, decayLearnRate=10, decayKernelWidth=20, errWindow=40
            , errThreshold=1e-6, seed=99)
        som.fit(show_plot=show_plot, verbose=False)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['Semi_Supervised']:
        #semi-supervised learning (include the target in the training data)
        scenarioName = 'Semi_Supervised'
        copy_df = df.copy()
        copy_df['target'] = target
        classificationAttributes = np.array([4])
        som = SOM(copy_df, scenarioName=scenarioName, classificationAttributes=classificationAttributes)
        som.fit(show_plot=show_plot)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['Fully_Supervised']:
        #fully-supervised learning (include the target in the training data)
        scenarioName = 'Fully_Supervised'
        copy_df = df.copy()
        copy_df['target'] = target
        som = SOM(copy_df, scenarioName=scenarioName)
        som.fit(show_plot=show_plot)

        labels = som.cluster(K=3, show_plot=show_plot)

    if ScenariosToRun['OtherFunctionality']:
        scenarioName = 'Base'
        som = SOM(df, load=True, scenarioName=scenarioName)

        #load model and clusters and plot the features and clusters
        som.loadModel()
        som.loadClusterLabels()
        labels = som.assignClusterLabels()

        #plot the Som map subsequently
        for f in range(som.n_features):
            som.plotMap(f, 'Blues', show_plot=True)

        #plot clusters
        som.plotMap(toPlot=som.mapClusters, FeatureName='Clusters', show_plot=True)#Note that som.mapClusters represents the cluster labels for each training record

        #use Silhouette score to optimise number of clusters (it seems to always recommend very few numebrs of clusters - not certain this is a reliable technique)
        som.optimiseCluster(maxK=10)
